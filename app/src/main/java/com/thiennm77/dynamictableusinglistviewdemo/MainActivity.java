package com.thiennm77.dynamictableusinglistviewdemo;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import com.thiennm77.dynamictableusinglistviewdemo.aliasmodel.AliasModelAdapter;
import com.thiennm77.dynamictableusinglistviewdemo.aliasmodel.AliasModelUiModel;
import com.thiennm77.dynamictableusinglistviewdemo.dto.Shop;
import com.thiennm77.dynamictableusinglistviewdemo.shop.ShopsPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements AbsListView.OnScrollListener, View.OnTouchListener {

    @BindView(R.id.alias_model)
    ListView aliasModelList;
    @BindView(R.id.data)
    ViewPager dataTable;

    private AliasModelAdapter aliasModelAdapter;
    private ShopsPagerAdapter pagerAdapter;
    public static ListView touchSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initAliasModelList();
        initDataTable();
        synchronizeScrolling();
    }

    private void initAliasModelList() {
        aliasModelAdapter = new AliasModelAdapter(this, AliasModelUiModel.uiModels);
        aliasModelList.setAdapter(aliasModelAdapter);
    }

    private void initDataTable() {
        pagerAdapter = new ShopsPagerAdapter(this, Shop.shops, AliasModelUiModel.uiModels, aliasModelList);
        dataTable.setAdapter(pagerAdapter);
    }

    private void synchronizeScrolling() {
        aliasModelList.setOnScrollListener(this);
        aliasModelList.setOnTouchListener(this);
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        View v = view.getChildAt(0);
        if (v != null && touchSource == aliasModelList) {
            pagerAdapter.setSelectionFromTop(aliasModelAdapter.getFirstVisibleModel(firstVisibleItem), v.getTop());
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        touchSource = aliasModelList;
        return false;
    }
}
