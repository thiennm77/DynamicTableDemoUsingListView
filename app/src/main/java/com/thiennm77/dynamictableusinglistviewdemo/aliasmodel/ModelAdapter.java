package com.thiennm77.dynamictableusinglistviewdemo.aliasmodel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thiennm77.dynamictableusinglistviewdemo.R;
import com.thiennm77.dynamictableusinglistviewdemo.dto.Model;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by thien on 3/11/2017.
 */

public class ModelAdapter extends BaseAdapter {

    private ArrayList<Model> data;
    private LayoutInflater inflater;

    public ModelAdapter(Context ctx, ArrayList<Model> data) {
        this.data = data;
        inflater =  LayoutInflater.from(ctx);
    }

    public void updateData(ArrayList<Model> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_model, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Model model = data.get(position);
        holder.model.setText(model.getName());

        return convertView;
    }

    static class ViewHolder {

        @BindView(R.id.model)
        TextView model;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

    }
}
