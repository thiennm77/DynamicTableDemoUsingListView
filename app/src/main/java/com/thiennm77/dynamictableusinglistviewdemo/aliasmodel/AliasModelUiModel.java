package com.thiennm77.dynamictableusinglistviewdemo.aliasmodel;

import com.thiennm77.dynamictableusinglistviewdemo.dto.Alias;
import com.thiennm77.dynamictableusinglistviewdemo.dto.Model;

import java.util.ArrayList;

/**
 * Created by thien on 2/11/2017.
 */

public class AliasModelUiModel {

    public static ArrayList<AliasModelUiModel> uiModels;

    static {
        uiModels = new ArrayList<>();

        ArrayList<Model> models = new ArrayList<>();
        models.add(new Model("Note8 vàng"));
        models.add(new Model("Note8 đen"));
        uiModels.add(new AliasModelUiModel(new Alias("Note8"), models));

        ArrayList<Model> models2 = new ArrayList<>();
        models2.add(new Model("S7Edge xanh"));
        models2.add(new Model("S7Edge đỏ"));
        models2.add(new Model("S7Edge đen"));
        uiModels.add(new AliasModelUiModel(new Alias("S7Edge"), models2));

        ArrayList<Model> models3 = new ArrayList<>();
        models3.add(new Model("TabA2017 trắng"));
        uiModels.add(new AliasModelUiModel(new Alias("TabA2017"), models3));

        ArrayList<Model> models4 = new ArrayList<>();
        models4.add(new Model("S9 xanh"));
        models4.add(new Model("S9 đỏ"));
        models4.add(new Model("S9 đen"));
        models4.add(new Model("S9 trắng"));
        models4.add(new Model("S9 hồng"));
        models4.add(new Model("S9 vàng"));
        models4.add(new Model("S9 nâu"));
        models4.add(new Model("S9 tím"));
        models4.add(new Model("S9 cam"));
        uiModels.add(new AliasModelUiModel(new Alias("S9"), models4));

    }

    private Alias alias;
    private ArrayList <Model> models;

    public Alias getAlias() {
        return alias;
    }

    public ArrayList<Model> getModels() {
        return models;
    }

    public AliasModelUiModel(Alias alias, ArrayList<Model> models) {
        this.alias = alias;
        this.models = models;
    }
}
