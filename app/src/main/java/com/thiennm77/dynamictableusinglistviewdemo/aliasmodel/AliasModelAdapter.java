package com.thiennm77.dynamictableusinglistviewdemo.aliasmodel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.thiennm77.dynamictableusinglistviewdemo.R;
import com.thiennm77.dynamictableusinglistviewdemo.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by thien on 3/11/2017.
 */

public class AliasModelAdapter extends BaseAdapter {

    private ArrayList<AliasModelUiModel> data;
    private ArrayList<ModelAdapter> adapters;
    private LayoutInflater inflater;

    public AliasModelAdapter(Context ctx, ArrayList<AliasModelUiModel> data) {
        this.data = data;
        inflater =  LayoutInflater.from(ctx);

        adapters = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            AliasModelUiModel model = data.get(i);
            adapters.add(new ModelAdapter(ctx, model.getModels()));
        }
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_alias_model, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        AliasModelUiModel model = data.get(position);
        holder.alias.setText(model.getAlias().getName());
        holder.modelList.setAdapter(adapters.get(position));
        setDesiredHeight(convertView.getContext(), position, holder.root);
        return convertView;
    }

    private void setDesiredHeight(Context context, int position, View item) {
        int desiredHeight = (int) (data.get(position).getModels().size() * (context.getResources().getDimension(R.dimen.item_height) + 1)) + 1; // 1 for divider
        ViewGroup.LayoutParams params = item.getLayoutParams();
        params.height = desiredHeight;
        item.setLayoutParams(params);
    }

    public int getFirstVisibleModel(int firstVisibleItem) {
        int result = 0;
        while (firstVisibleItem > 0) {
            result += data.get(firstVisibleItem -1).getModels().size();
            firstVisibleItem--;
        }
        return result;
    }

    static class ViewHolder {

        @BindView(R.id.alias)
        TextView alias;
        @BindView(R.id.models)
        ListView modelList;
        @BindView(R.id.root)
        View root;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
