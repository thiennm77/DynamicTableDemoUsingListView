package com.thiennm77.dynamictableusinglistviewdemo.dto;

import java.util.ArrayList;

/**
 * Created by thien on 2/11/2017.
 */

public class Shop {

    public static ArrayList<Shop> shops;

    static {
        shops = new ArrayList<>();
        shops.add(new Shop("GR00000053"));
        shops.add(new Shop("GR00000051"));
        shops.add(new Shop("GR00000052"));
        shops.add(new Shop("GR00000054"));
    }

    private String code;

    public String getCode() {
        return code;
    }

    public Shop(String code) {
        this.code = code;
    }
}
