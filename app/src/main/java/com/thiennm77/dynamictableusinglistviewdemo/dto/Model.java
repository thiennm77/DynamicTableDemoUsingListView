package com.thiennm77.dynamictableusinglistviewdemo.dto;

/**
 * Created by thien on 2/11/2017.
 */

public class Model {

    private String name;

    public String getName() {
        return name;
    }

    public Model(String name) {
        this.name = name;
    }
}
