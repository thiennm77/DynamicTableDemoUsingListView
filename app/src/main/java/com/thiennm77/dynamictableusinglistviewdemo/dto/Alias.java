package com.thiennm77.dynamictableusinglistviewdemo.dto;

/**
 * Created by thien on 2/11/2017.
 */

public class Alias {

    private String name;

    public String getName() {
        return name;
    }

    public Alias(String name) {
        this.name = name;
    }
}
