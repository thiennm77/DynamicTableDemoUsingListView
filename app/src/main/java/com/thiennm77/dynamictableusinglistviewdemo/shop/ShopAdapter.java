package com.thiennm77.dynamictableusinglistviewdemo.shop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.thiennm77.dynamictableusinglistviewdemo.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by thien on 3/11/2017.
 */

public class ShopAdapter extends BaseAdapter {

    private ArrayList<String> data;
    private LayoutInflater inflater;

    public ShopAdapter(Context context, ArrayList<String> data) {
        this.data = data;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_shop, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String value = data.get(position);
        holder.text.setText(value);

        return convertView;
    }


    static class ViewHolder {

        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.root)
        View root;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
