package com.thiennm77.dynamictableusinglistviewdemo.shop;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.thiennm77.dynamictableusinglistviewdemo.MainActivity;
import com.thiennm77.dynamictableusinglistviewdemo.R;
import com.thiennm77.dynamictableusinglistviewdemo.Utils;
import com.thiennm77.dynamictableusinglistviewdemo.aliasmodel.AliasModelUiModel;
import com.thiennm77.dynamictableusinglistviewdemo.dto.Model;
import com.thiennm77.dynamictableusinglistviewdemo.dto.Shop;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by thien on 3/11/2017.
 */

public class ShopsPagerAdapter extends PagerAdapter implements AbsListView.OnScrollListener, View.OnTouchListener {

    private ArrayList<Shop> data;
    private ArrayList<Model> models;
    private float widthRatio;
    private LayoutInflater inflater;

    // for scrolling sync
    private ArrayList<ListView> listViews;
    private ListView aliasModelList;

    public ShopsPagerAdapter(Context context, ArrayList<Shop> data, ArrayList<AliasModelUiModel> aliasModels, ListView aliasModelList) {
        this.data = data;
        this.models = getModels(aliasModels);
        this.aliasModelList = aliasModelList;
        float dim = context.getResources().getDimension(R.dimen.item_width);
        widthRatio = dim / getPagerWidthInPx(context);
        inflater =  LayoutInflater.from(context);
        listViews = new ArrayList<>(data.size());
    }

    private float getPagerWidthInPx(Context context) {
        return Utils.getScreenWidthInPx(context) - context.getResources().getDimension(R.dimen.alias_model_width);
    }

    private ArrayList<Model> getModels(ArrayList<AliasModelUiModel> aliasModels) {
        ArrayList<Model> result = new ArrayList<>();
        for (AliasModelUiModel uiModel : aliasModels) {
            result.addAll(uiModel.getModels());
        }
        return  result;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.list_shop, container, false);
        ListView list = layout.findViewById(R.id.list);
        TextView shopName = layout.findViewById(R.id.shop_name);

        Shop shop = data.get(position);

        shopName.setText(shop.getCode());

        ArrayList<String> shopData = getShopData();
        ShopAdapter adapter = new ShopAdapter(container.getContext(), shopData);
        list.setAdapter(adapter);

        list.setOnTouchListener(this);
        //list.setOnScrollListener(this);

        listViews.add(position, list);
        container.addView(layout);
        return layout;
    }

    private ArrayList<String> getShopData() {
        ArrayList<String> result = new ArrayList<>();
        Random random = new Random(System.currentTimeMillis());
        for (int i = 0; i < models.size(); i++) {
            //result.add(String.valueOf(random.nextInt(2)));
            result.add(String.valueOf(i + 1));
        }
        return result;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public float getPageWidth(int position) {
        return widthRatio;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public void setSelectionFromTop(int firstVisibleItem, int top) {
        for (ListView listView : listViews) {
            if (listView != MainActivity.touchSource) {
                listView.setSelectionFromTop(firstVisibleItem, top);
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        MainActivity.touchSource = (ListView) v;
        aliasModelList.dispatchTouchEvent(event);
        return false;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        View v = view.getChildAt(0);
        if (v != null && MainActivity.touchSource == view) {
            aliasModelList.setSelectionFromTop(firstVisibleItem, v.getTop());
            setSelectionFromTop(firstVisibleItem, v.getTop());
        }
    }
}
